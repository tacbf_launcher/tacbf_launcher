# The project has been moved!

This project has been moved and is now hosted on Github as [Bulletproof Arma Launcher](https://github.com/overfl0/Bulletproof-Build-Environment)

The source code that is available here is old, contains bugs and will not be updated anymore. Please switch to the repository on Github.